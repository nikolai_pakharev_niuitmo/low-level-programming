#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err34-c"

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "linked_list.h"
#include "list_high_order_functions.h"

void print(ListDataType x, const char separator) {
  printf("%d%c", x, separator);
}

void print_with_space_as_separator(ListDataType x) {
  return print(x, ' ');
}

void print_with_newline_symbol_as_separator(ListDataType x) {
  return print(x, '\n');
}

ListDataType add(ListDataType x, ListDataType y) {
  return x + y;
}

ListDataType print_square_with_space_as_separator(ListDataType x) {
  ListDataType kSquareOfX = x * x;
  print_with_space_as_separator(kSquareOfX);
  return kSquareOfX;
}

ListDataType print_cube_with_space_as_separator(ListDataType x) {
  ListDataType kCubeOfX = x * x * x;
  print_with_space_as_separator(kCubeOfX);
  return kCubeOfX;
}

int main() {
  List list = { NULL };
  int value;
  int scanf_returned = scanf("%d", &value);
  if (scanf_returned == 1) {
    list = list_create(value);

    do {
      scanf_returned = scanf("%d", &value);
      if (scanf_returned != 1) {
        break;
      }
      list_add_front(&list, value);
    } while (true);
  }

  foreach(list, &print_with_space_as_separator);
  printf("\n");
  foreach(list, &print_with_newline_symbol_as_separator);
  printf("\n");

  map(list, &print_square_with_space_as_separator);
  printf("\n");
  map(list, &print_cube_with_space_as_separator);
  printf("\n");

  ListDataType sum = foldl(0, &add, list);
  printf("%d\n", sum);

  list_free(list);
}

#pragma clang diagnostic pop