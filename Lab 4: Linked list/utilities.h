#ifndef LOW_LEVEL_UTILITIES_H
#define LOW_LEVEL_UTILITIES_H

void print_error_message_and_exit(const char* err_msg);

#endif  // LOW_LEVEL_UTILITIES_H