#include "linked_list.h"

#include <stdlib.h>
#include <iso646.h>

#include "utilities.h"

ListNode* list_create_node(const bool next_is_null) {
  ListNode* new_node = (next_is_null
                        ? ((ListNode*) calloc(sizeof(ListNode), 1))
                        : ((ListNode*) malloc(sizeof(ListNode))));
  return new_node;
}

List* list_create(const ListDataType n) {
  List* new_list = ((List*) malloc(sizeof(List)));
  new_list->root = list_create_node(true);
  new_list->root->value = n;
  return new_list;
}

bool list_is_empty(const List* const list) {
  if (list == NULL) {
    print_error_message_and_exit("list_is_empty(list) got NULL as the input parameter");
  }
  return list->root == NULL;
}

ListNode* list_try_advance(ListNode* const node, const size_t steps) {
  if (node == NULL) {
    return NULL;
  }

  ListNode* res = node;
  for (size_t i = 0; i < steps; ++i) {
    res = res->next;
    if (res == NULL) {
      return res;
    }
  }
  return res;
}

ListNode* list_advance_unsafe(ListNode* const node, const size_t steps) {
  ListNode* curr = node;
  for (size_t i = 0; i < steps; ++i) {
    curr = curr->next;
  }
  return curr;
}

ListNode* list_advance(ListNode* const node, const size_t steps) {
  ListNode* res = list_try_advance(node, steps);
  if (res == NULL) {
    print_error_message_and_exit("advance(steps, list_node) failed");
  }
  return res;
}

void list_add_front(List* const list, const ListDataType n) {
  ListNode* new_node = ((ListNode*) malloc(sizeof(ListNode)));
  new_node->value = n;
  new_node->next = list->root;
  list->root = new_node;
}

ListNode list_get_ptr_to_last_entry_and_length(const List* list) {
  ListNode res;
  if (list_is_empty(list)) {
    res.value = 0;
    res.next = NULL;
    return res;
  }

  res.value = 1;
  res.next = list->root;
  while (true) {
    ListNode* try_advance_result = list_try_advance(res.next, 1);
    if (try_advance_result == NULL) {
      return res;
    }
    res.value++;
    res.next = try_advance_result;
  }
}

void list_add_back(List* const list, const ListDataType n) {
  ListNode ptr_to_last_entry_and_length = list_get_ptr_to_last_entry_and_length(list);
  ptr_to_last_entry_and_length.next = list_create_node(n);
}

void list_free_node_rec(ListNode* node) {
  if (node != NULL) {
    list_free_node_rec(node->next);
  }
  free(node);
}

void list_free(List* const list) {
  list_free_node_rec(list->root);
  list->root = NULL;
}

size_t list_length(const List* const list) {
  ListNode ptr_to_last_entry_and_length = list_get_ptr_to_last_entry_and_length(list);
  return ptr_to_last_entry_and_length.value;
}

ListNode* list_node_at(List* const list, const size_t idx) {
  ListNode* res = list_try_advance(list->root, idx);
  return res;
}

ListDataType list_get(List* const list, const size_t idx) {
  ListNode* res = list_node_at(list, idx);
  if (res) {
    return res->value;
  }
  return 0;
}

intmax_t list_node_sum_rec(const ListNode* const node) {
  if (node) {
    return node->value + list_node_sum_rec(node->next);
  }
  return 0;
}

intmax_t list_sum(const List* const list) {
  if (list_is_empty(list)) {
    print_error_message_and_exit("list_sum(list) got empty list as the input parameter");
  }
  return list_node_sum_rec(list->root);
}