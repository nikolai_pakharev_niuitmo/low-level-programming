#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err34-c"

#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"

int main() {
  List* list = NULL;
  int value;
  int scanf_returned = scanf("%d", &value);
  if (scanf_returned == 1) {
    list = list_create(value);

    do {
      scanf_returned = scanf("%d", &value);
      if (scanf_returned != 1) {
        break;
      }
      list_add_front(list, value);
    } while (true);
  }

  printf("sum = %ld\n", list_sum(list));
  list_free(list);
}

#pragma clang diagnostic pop